import time
import random
import os


on = chr(9632)
off = chr(9633)


def cell_state(board, col, row):
    if row in range(len(board)) and col in range(len(board[0])):
        return 1 if board[row][col] else 0
    return 0


def num_neighbours(board, col, row):
    num = 0
    for i in (-1, 0, 1):
        for j in (-1, 0, 1):
            if i or j:
                num += cell_state(board, col + i, row + j)
    return num


def create_board(cols, rows, seed=time.time()):
    random.seed(seed)
    return [[random.choice([True, False]) for col in range(cols)] for row in range(rows)]


def new_state(board, col, row):
    neighbours = num_neighbours(board, col, row)
    if board[col][row]:
        return neighbours == 2 or neighbours == 3
    else:
        return neighbours == 3


def new_board(board, cols, rows):
    return [[new_state(board, col, row) for row in range(cols)] for col in range(rows)]


def print_board(board):
    for row in board:
        for e in row:
            print(on+" " if e else off+" ", end='')
        print()


def end_check(board):
    for col in board:
        for e in col:
            if not e:
                return True
    return False


def main():
    cols = int(input('Enter board width: '))
    rows = int(input('Enter board height: '))
    seed = input('Enter board number(can be left blank): ')
    if seed:
        board = create_board(cols, rows, float(seed))
    else:
        board = create_board(cols, rows)
    gen = 0
    while True:
        os.system("cls" if os.name == 'nt' else 'clear')
        print_board(board)
        print('gen:', gen)
        # if not end_check(board):
        #     break
        gen += 1
        board = new_board(board, cols, rows)
        time.sleep(1)


main()
